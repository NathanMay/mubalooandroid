package nathanmay.mubalootest;

import android.content.Context;
import android.support.annotation.NonNull;
import android.util.Log;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.net.FileNameMap;

/**
 * Created by Nathan on 12/03/2018.
 * Caches or Retrieves cached JSON data in the event that network is unavailable.
 */

public class JSONCacher {


    /**
     * Returns a cached version of the JSON file if it is present.
     * @param context
     * @return JSON string if available.
     */
    public String getCachedJSON(Context context) {
        try {
            String filename = context.getResources().getString(R.string.cache_filename);
            FileInputStream iStream = context.openFileInput(filename);
            InputStreamReader iStreamReader = new InputStreamReader(iStream);
            BufferedReader bufferedReader = new BufferedReader(iStreamReader);
            StringBuilder builder = new StringBuilder();
            String cachedLine;
            while((cachedLine = bufferedReader.readLine()) != null){
                builder.append(cachedLine);
            }
            return builder.toString();

        }catch (FileNotFoundException e){
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return null;

    }

    /**
     * Writes the JSON file to disk if it can be found.
     * @param context
     * @param JSONString
     * @return
     */

    public boolean cacheJSON(Context context, String JSONString){
        try {
            String filename = context.getResources().getString(R.string.cache_filename);
            OutputStreamWriter oStream = new OutputStreamWriter(context.openFileOutput(filename, Context.MODE_PRIVATE));
            oStream.write(JSONString);
            oStream.close();

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        Log.d("cache", "File written to " + context.getFilesDir());
        return true;
    }
}
