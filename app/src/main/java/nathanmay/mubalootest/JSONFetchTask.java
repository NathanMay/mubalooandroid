package nathanmay.mubalootest;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;
import android.view.View;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Map;

/**
 * Created by Nathan on 12/03/2018.
 * Fetches the JSON data in the background.
 */

public class JSONFetchTask extends AsyncTask<String, String, JSONObject> {

    private Context mContext;
    private String mJSONDataString;
    private final String ID_STRING = "id";
    private final String FIRST_NAME_STRING = "firstName";
    private final String LAST_NAME_STRING = "lastName";
    private final String ROLE_STRING = "role";
    private final String IMAGE_URL = "profileImageURL";
    private final String TEAM_LEAD = "teamLead";
    private boolean mTaskComplete = false;
    private HttpURLConnection mUrlConnection;
    private Map<String, TeamMember> mMemberMap;
    private ArrayList<TeamMember> mMemberArray;
    private JSONArray mJSONTeamArray;




    //Constructor
    public JSONFetchTask(Context context){
        mContext = context;
    }

    public boolean ismTaskComplete() {
        return mTaskComplete;
    }
    @Override
    protected void onPreExecute() {

        super.onPreExecute();
    }

    /**
     * Returns the list of team members, sorted by Team.
     * @return
     */
    public ArrayList<TeamMember> getmMemberArray() {
        if(mMemberArray != null){
            Collections.sort(mMemberArray, new Comparator<TeamMember>() {
                @Override
                public int compare(TeamMember teamMember, TeamMember t1) {
                    return teamMember.getmTeamName().compareTo(t1.getmTeamName());
                }
            });
        }
        return mMemberArray;
    }

    /**
     * Performs and ASynchronous Task, gathering data from the provided URL and
     * creating a continuous String from it.
     * @param strings
     * @return
     */
    @Override
    protected JSONObject doInBackground(String... strings) {
        String url = mContext.getResources().getString(R.string.connection_url);
        try {
            URL teamURL = new URL(url);
            HttpURLConnection urlConnection = (HttpURLConnection) teamURL.openConnection();
            //Read data from the URL
            InputStream iStream = urlConnection.getInputStream();
            BufferedReader iReader = new BufferedReader(new InputStreamReader(iStream));
            String lineIn = ""; //Empty string to store line
            mJSONDataString = "";

            StringBuilder output = new StringBuilder();
            while((lineIn = iReader.readLine()) != null){
                output.append(lineIn);
            }
            iReader.close();
            //Convert to string
            mJSONDataString = output.toString();
            //Remove all of the whitespace from the data.
            mJSONDataString = mJSONDataString.replaceAll("\\s+", "");
            //Create JSON Array of the data gathered.
            mJSONTeamArray = new JSONArray(mJSONDataString);
            //Get the first object, which is the CEO.
            //Initialize the member array list
            mMemberArray = new ArrayList<>();


            setMembersData();


        }catch(MalformedURLException e){
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return null;
    }

    /**
     * Performs the retrieval of Team information from the JSON array.
     * It then adds Team Members, sequentially, to the Member Array.
     * @throws JSONException
     */
    public void setMembersData() throws JSONException {
        setTeam(1);
        setTeam(2);
        setTeam(3);
        setTeam(4);
        setIndividual(0);

    }

    /**
     * Gets the data for an entire team at a given index, adding members to the array.
     * @param index
     * @throws JSONException
     */
    private void setTeam(int index) throws JSONException {
        JSONObject JSONTEAM = mJSONTeamArray.getJSONObject(index);
        JSONArray teamArray = JSONTEAM.getJSONArray("members");
        String teamName = JSONTEAM.getString("teamName");

        for(int i = 0; i < teamArray.length(); i++){
            TeamMember employee = getTeamMemberObject(teamName, teamArray.getJSONObject(i));
            mMemberArray.add(employee);

        }
    }


    /**
     * Gets the data for an individual at an index, checks if that individual is the CEO,
     * and then adds to the member list.
     * @param index
     * @throws JSONException
     */
    private void setIndividual(int index) throws JSONException {
        JSONObject JSONCEO = mJSONTeamArray.getJSONObject(index);
        //The only member with no team would be the CEO.
        String teamName = "CEO";
        TeamMember CEO = getTeamMemberObject(teamName, JSONCEO);
        mMemberArray.add(CEO);
    }


    /**
     * Performs a conversion between the JSONObject and TeamMember.
     * @param teamName
     * @param jsonObject
     * @return TeamMember object based on JSONObject input
     * @throws JSONException
     */
    private TeamMember getTeamMemberObject(String teamName, JSONObject jsonObject) throws
            JSONException {
        String id = jsonObject.getString(ID_STRING);
        String firstname = jsonObject.getString(FIRST_NAME_STRING);
        String lastname = jsonObject.getString(LAST_NAME_STRING);
        String role = jsonObject.getString(ROLE_STRING);
        String imageURL = jsonObject.getString(IMAGE_URL);
        boolean notTeamLead = jsonObject.isNull(TEAM_LEAD);

        if(!notTeamLead){
            boolean teamlead = true;
            return new TeamMember(id, teamName, firstname, lastname, role, imageURL, teamlead);
        }else{
            return new TeamMember(id, teamName, firstname, lastname, role, imageURL);
        }




    }


    /**
     * Runs when the background task is complete. Handles cache checks if
     * there is no data available via the network.
     * @param jsonObject
     */
    @Override
    protected void onPostExecute(JSONObject jsonObject) {
        JSONCacher cacher = new JSONCacher();
        if (mMemberArray == null && cacher.getCachedJSON(mContext) == null) {
            MainActivity.mNoDataView.setVisibility(View.VISIBLE);

        }else if (mMemberArray == null){
            try {
                mJSONDataString = cacher.getCachedJSON(mContext);
                //Create JSON Array of the data gathered.
                mJSONTeamArray = new JSONArray(mJSONDataString);
                mMemberArray = new ArrayList<>();
                setMembersData();
                mTaskComplete = true;

            } catch (JSONException e) {
                e.printStackTrace();
            }

        }else{
            mTaskComplete = true;
            cacher.cacheJSON(mContext, mJSONDataString);

        }

        super.onPostExecute(jsonObject);
    }



}
