package nathanmay.mubalootest;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;

/*
Crown icon made by "http://www.freepik.com"
from "https://www.flaticon.com/"
licensed by "http://creativecommons.org/licenses/by/3.0/"
 */

/**
 * Created by Nathan on 12/03/2018.
 */
public class MainActivity extends AppCompatActivity {


    private ListView mMainListView;
    public static TextView mNoDataView;
    public static TeamMember mMemberTapped;

    /**
     * Runs as soon as the activity is launched, and the JSONFetchTask
     * begins checking for data.
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        mNoDataView = findViewById(R.id.no_cache);

        //This will get all the data from the JSON file in the background
        //It will then create an arrayList with formatted data.
        JSONFetchTask fetchTeamMembers = new JSONFetchTask(getApplicationContext());
        fetchTeamMembers.execute();

        mMainListView = findViewById(R.id.mainListView);

        checkForData(fetchTeamMembers);

    }

    /**
     * Check for data that has come from the background task.
     * This will check every half-second, and if complete will stop checking.
     * @param runningTask
     */
    private void checkForData(final JSONFetchTask runningTask) {
        final Handler handler = new Handler();
        final Runnable checker = new Runnable() {
            @Override
            public void run() {
                if(runningTask.ismTaskComplete()){
                    ArrayList<TeamMember> memberArray = runningTask.getmMemberArray();
                    attachListViewData(memberArray);
                    mNoDataView.setVisibility(View.INVISIBLE);
                    handler.removeCallbacks(this);

                }

            }
        };
        handler.postDelayed(checker, 500);

    }

    /**
     * Takes data from the created arrayList and populates the ListView.
     * It also sets an onItemClickListener for each element in the list.
     * @param teamMemberArrayList
     */
    private void attachListViewData(ArrayList<TeamMember> teamMemberArrayList) {
        for(int i = 0; i < teamMemberArrayList.size(); i++){
            TeamMember checkedMember = teamMemberArrayList.get(i);
            //This moves the CEO to the top - this just made more sense to me.
            if(checkedMember.getmRole().equals("CEO")){
                int index = teamMemberArrayList.indexOf(checkedMember);
                teamMemberArrayList.remove(index);
                teamMemberArrayList.add(0, checkedMember);
            }
        }
        TeamArrayAdapter adapter = new TeamArrayAdapter(this, R.layout.listview_row,
                teamMemberArrayList);
        mMainListView.setAdapter(adapter);

        mMainListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                TeamMember memberTapped = (TeamMember)adapterView.getItemAtPosition(i);
                mMemberTapped = memberTapped;
                Log.d("test", "you tapped " + memberTapped.getmFirstName());

                Intent intent = new Intent(getApplicationContext(), MemberDetailActivity.class);
                startActivity(intent);

            }
        });

    }

    /**
     * Creates the one-item options menu with the refresh button.
     * @param menu
     * @return
     */
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        menu.getItem(0).setIcon(ContextCompat.getDrawable(this, R.drawable.ic_refresh));
        return true;

    }

    /**
     * Handles the Refresh Button click.
     * @param item
     * @return
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        //Handles the refresh button.
        if (id == R.id.action_refresh) {
            JSONFetchTask retryFetch = new JSONFetchTask(getApplicationContext());
            retryFetch.execute();
            checkForData(retryFetch);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }



}
