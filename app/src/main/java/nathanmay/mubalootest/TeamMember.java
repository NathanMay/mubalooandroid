package nathanmay.mubalootest;

/**
 * Created by Nathan on 12/03/2018.
 * Provides the data model for every team member within Mubaloo.
 */

public class TeamMember {

    private String mID;
    private String mTeamName;
    private String mFirstName;
    private String mLastName;
    private String mRole;
    private String mImageURL;
    private boolean mTeamLead;



    public TeamMember(String id, String teamName, String first, String last, String role, String
            imageURL){
        mID = id;
        mTeamName = teamName;
        mFirstName = first;
        mLastName = last;
        mRole = role;
        mImageURL = imageURL;
    }
    //Overloaded Constructor
    public TeamMember(String id, String teamName, String first, String last, String role, String
            imageURL,
                      boolean leader){
        mID = id;
        mTeamName = teamName;
        mFirstName = first;
        mLastName = last;
        mRole = role;
        mImageURL = imageURL;
        mTeamLead = leader;
    }

    public String getmTeamName() {
        return mTeamName;
    }

    public void setmTeamName(String mTeamName) {
        this.mTeamName = mTeamName;
    }

    public String getmID() {
        return mID;
    }

    public void setmID(String mID) {
        this.mID = mID;
    }

    public String getmFirstName() {
        return mFirstName;
    }

    public void setmFirstName(String mFirstName) {
        this.mFirstName = mFirstName;
    }

    public String getmLastName() {
        return mLastName;
    }

    public void setmLastName(String mLastName) {
        this.mLastName = mLastName;
    }

    public String getmRole() {
        return mRole;
    }

    public void setmRole(String mRole) {
        this.mRole = mRole;
    }

    public String getmImageURL() {
        return mImageURL;
    }

    public void setmImageURL(String mImageURL) {
        this.mImageURL = mImageURL;
    }

    public boolean ismTeamLead() {
        return mTeamLead;
    }

    public void setmTeamLead(boolean mTeamLead) {
        this.mTeamLead = mTeamLead;
    }
}
