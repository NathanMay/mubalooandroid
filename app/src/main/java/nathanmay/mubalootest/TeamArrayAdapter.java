package nathanmay.mubalootest;

import android.app.Activity;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.text.Layout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;


/**
 * Created by Nathan on 12/03/2018.
 * Acts as a custom adapter for the listview seen in the main activity.
 */
public class TeamArrayAdapter extends ArrayAdapter<TeamMember> {

    private ArrayList<TeamMember> mArrayList;
    private Context mContext;
    private int mLayoutID;

    //Default Constructor
    public TeamArrayAdapter(@NonNull Context context, int resourceID) {
        super(context, resourceID);
    }

    //Used Constructor.
    public TeamArrayAdapter(@NonNull Context context, int resourceID, ArrayList<TeamMember> teamMembers) {
        super(context, resourceID, teamMembers);
        mArrayList = teamMembers;
        mContext = context;
        mLayoutID = resourceID;

    }

    /**
     * Populates the ListView with inflated views for each Team Member.
     * @param position
     * @param convertView
     * @param parent
     * @return
     */
    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        View view;
        ViewHolder rowHolder;

        int leaderColour = ContextCompat.getColor(mContext, R.color.isLeader);
        int CEOColour = ContextCompat.getColor(mContext, R.color.isCEO);

        LayoutInflater inflater = ((Activity)mContext).getLayoutInflater();
        view = inflater.inflate(mLayoutID, parent, false);

        rowHolder  = new ViewHolder();
        rowHolder.nameText = view.findViewById(R.id.listview_name);
        rowHolder.teamText = view.findViewById(R.id.listview_team);
        TeamMember employee = mArrayList.get(position);
        rowHolder.nameText.setText(employee.getmFirstName() + " " + employee.getmLastName());
        rowHolder.teamText.setText(employee.getmTeamName());
        if(employee.ismTeamLead()){
            view.setBackgroundColor(leaderColour);
            //Highlight CEO in a similar fashion to the team leaders.
        }else if(employee.getmRole().equals("CEO")){
            view.setBackgroundColor(CEOColour);
        }
        return view;

    }

    //A simple view holder for each ListView element.
    static class ViewHolder {

        TextView nameText;
        TextView teamText;

    }

}
