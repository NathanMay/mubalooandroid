package nathanmay.mubalootest;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;

/**
 * Created by Nathan on 12/03/2018.
 * The activity to handle Member Details in a more appealing way.
 */

public class MemberDetailActivity extends AppCompatActivity {

    public ImageView mProfilePic;
    public ImageView mLeaderIcon;
    public ImageView mCEOIcon;

    public TextView mMemberID;
    public TextView mMemberNameFirst;
    public TextView mMemberRole;
    public TextView mMemberNameLast;
    public TextView mMemberTeamName;

    public TeamMember mTappedMember;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);

        mProfilePic = findViewById(R.id.profilePic_view);
        mLeaderIcon = findViewById(R.id.leader_image);
        mCEOIcon = findViewById(R.id.ceo_image);

        mMemberID = findViewById(R.id.id_view);
        mMemberNameFirst = findViewById(R.id.first_name_view);
        mMemberRole = findViewById(R.id.role_view);
        mMemberNameLast = findViewById(R.id.last_name_view);
        mMemberTeamName = findViewById(R.id.team_name_view);

        mTappedMember = MainActivity.mMemberTapped;
        if(mTappedMember.ismTeamLead()){
            mLeaderIcon.setVisibility(View.VISIBLE);
        }
        if(mTappedMember.getmRole().equals("CEO")){
            mCEOIcon.setVisibility(View.VISIBLE);
        }

        mMemberID.setText("Employee ID:  " + mTappedMember.getmID());
        mMemberNameFirst.setText("First Name: " + mTappedMember.getmFirstName());
        mMemberNameLast.setText("Last Name: " + mTappedMember.getmLastName());
        mMemberRole.setText("Role: " + mTappedMember.getmRole());
        mMemberTeamName.setText("Team: " + mTappedMember.getmTeamName());

        //Get the profile picture and update the UI.
        new PictureRetriever().execute();

    }


    /**
     * Sets the profile picture via the url for a given team member
     * This uses a thread as network jobs shouldn't be completed on the main thread.
     */
    private class PictureRetriever extends AsyncTask {

        private Bitmap mRetrievedPicture;
        @Override
        protected Object doInBackground(Object[] objects) {
            URL imageURL = null;
            try {
                imageURL = new URL(mTappedMember.getmImageURL());
            } catch (MalformedURLException e) {
                e.printStackTrace();
            }
            try {
                mRetrievedPicture = BitmapFactory.decodeStream(imageURL.openConnection()
                        .getInputStream());
                //If a picture cant be found, show an error icon.
            } catch (IOException e) {
                mRetrievedPicture = BitmapFactory.decodeResource(getResources(), R.drawable
                        .ic_error_outline);
                e.printStackTrace();
            }


            return null;
        }

        //When an image is found, set it in the UI.
        @Override
        protected void onPostExecute(Object o) {
            mProfilePic.setImageBitmap(mRetrievedPicture);
            super.onPostExecute(o);
        }
    }


}
